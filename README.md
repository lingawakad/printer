# ï̴̦t̷̼̅'̷̙̀s̵̘͛ ̷͉̍a̶͇̐ ̷̪̀3̶͈̄d̴͍̈ ̷͇̽p̵̨͝r̵̢̋i̷̹͝n̸̤͠ṱ̴́e̸͈̓r̸͉̈́ ̴̞̈́c̵͚͂ǫ̵́n̵̼͌f̵̭̊i̸̫͒g̵͔͝

# printer
- creality cr-10v3

# mods
- [antclabs bl touch](https://www.antclabs.com/bltouch)
- [skr mini e3 v3](https://github.com/bigtreetech/BIGTREETECH-SKR-mini-E3)
- [microswiss ng direct drive](https://store.micro-swiss.com/collections/extruders/products/micro-swiss-ng-direct-drive-extruder)
- [0.6mm diamondback mk8-compatible nozzle](https://www.championx.com/products-and-solutions/drilling-technologies/diamondback-nozzles/)
- garolite build plate - so far 60c/190c bed/nozzle is working for [3dfuel pla](https://www.3dfuel.com/collections/175spla)
- i ripped the control box apart and printed separate boxes for the [board](https://www.printables.com/model/223029) and [psu](https://www.printables.com/model/338101-cr-10v3-meanwell-lrs-350-24-cover)

# software
- [prusa slicer](https://github.com/prusa3d/PrusaSlicer)
- [kiauh](https://github.com/th33xitus/kiauh) to set up [klipper](https://github.com/Klipper3d/klipper), [moonraker](https://github.com/Arksine/moonraker), [mainsail](https://github.com/mainsail-crew/mainsail)
- [Klipper Adaptive Meshing and Purging by kyleisah](https://github.com/kyleisah/Klipper-Adaptive-Meshing-Purging)

# some helpful links 
- [ellis' print tuning guide](https://ellis3dp.com/Print-Tuning-Guide/)
